package com.kingeshop.heavendeer.databindingpractice.userDetail;

import android.os.Parcel;
import android.os.Parcelable;

public class UserObject implements Parcelable {

    String name;
    String recent_msg;
    String image_url;

    public String getName() {
        return name;
    }

    public String getRecent_msg() {
        return recent_msg;
    }

    public String getImage_url() {
        return image_url;
    }


    protected UserObject(Parcel in) {
        name = in.readString();
        recent_msg = in.readString();
        image_url = in.readString();
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(name);
        dest.writeString(recent_msg);
        dest.writeString(image_url);
    }

    @SuppressWarnings("unused")
    public static final Parcelable.Creator<UserObject> CREATOR = new Parcelable.Creator<UserObject>() {
        @Override
        public UserObject createFromParcel(Parcel in) {
            return new UserObject(in);
        }

        @Override
        public UserObject[] newArray(int size) {
            return new UserObject[size];
        }
    };
}



