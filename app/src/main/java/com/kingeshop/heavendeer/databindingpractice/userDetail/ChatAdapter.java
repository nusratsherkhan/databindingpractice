package com.kingeshop.heavendeer.databindingpractice.userDetail;

import android.databinding.DataBindingUtil;
import android.databinding.ViewDataBinding;
import android.os.Parcelable;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.android.databinding.library.baseAdapters.BR;
import com.kingeshop.heavendeer.databindingpractice.R;
import com.kingeshop.heavendeer.databindingpractice.databinding.ChatLayoutBinding;

import java.util.List;

public class ChatAdapter<T extends Parcelable> extends RecyclerView.Adapter<ChatAdapter.ChatViewHolder>{
    private List<T> list;
    private int layoutID=0;

    public ChatAdapter(List<T> m,int id){
        layoutID=id;

        list= m;

    }


    @Override
    public ChatViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        ViewDataBinding chatLayoutBinding= DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()),layoutID,parent,false);
        return new ChatViewHolder(chatLayoutBinding);
    }

    @Override
    public void onBindViewHolder(ChatAdapter.ChatViewHolder holder, int position) {
        holder.bind.setVariable(BR.user,list.get(position));

    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    class ChatViewHolder extends RecyclerView.ViewHolder{
        ViewDataBinding bind;

        public ChatViewHolder(ViewDataBinding binding) {
            super(binding.getRoot());

           bind= binding;

        }
    }

}