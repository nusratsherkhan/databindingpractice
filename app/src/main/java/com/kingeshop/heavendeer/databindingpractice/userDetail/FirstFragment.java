package com.kingeshop.heavendeer.databindingpractice.userDetail;

import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.kingeshop.heavendeer.databindingpractice.R;
import com.kingeshop.heavendeer.databindingpractice.databinding.FirstFragmentBinding;

import java.util.List;

/**
 * Created by madan on 25/01/18.
 */

public class FirstFragment extends Fragment {
    FirstFragmentBinding binding;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {

      binding = DataBindingUtil.inflate(inflater, R.layout.first_fragment,container,false);



        getFFViewModel().fetchData();

        getFFViewModel().data.observe(this, new Observer<List<UserObject>>() {
            @Override
            public void onChanged(@Nullable List<UserObject> userObjects) {
                updateAdapter(userObjects);
            }
        });



        return binding.getRoot();
    }

    private void updateAdapter(List<UserObject> userObjects) {

        binding.ffRvId.setLayoutManager(new LinearLayoutManager(getActivity()));
        ChatAdapter chatAdapter = new ChatAdapter<UserObject>(userObjects,R.layout.chat_layout);

        binding.ffRvId.setAdapter(chatAdapter);

    }

    private FirstFragmentViewModel getFFViewModel(){
        return ViewModelProviders.of(this).get(FirstFragmentViewModel.class);
    }


}
