package com.kingeshop.heavendeer.databindingpractice.networking;

import com.kingeshop.heavendeer.databindingpractice.userDetail.UserObject;

import java.util.List;

import retrofit2.Call;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.http.GET;

public class HeavenDeerClient {
    private static HeavenDeerRestServe heavenDeerRestServe = null;

    private HeavenDeerClient() {
    }

    private Retrofit createAdapter() {
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl("https://demo1715650.mockable.io/")
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        return retrofit;
    }

    public static HeavenDeerRestServe getRestClient() {
        //new HeavenDeerClient().createAdapter();
        if (heavenDeerRestServe == null)
            heavenDeerRestServe = new HeavenDeerClient().createAdapter().create(HeavenDeerRestServe.class);

        return heavenDeerRestServe;
    }

    public interface HeavenDeerRestServe {
        @GET("chat")
        Call<List<UserObject>> loadchat();


    }


}