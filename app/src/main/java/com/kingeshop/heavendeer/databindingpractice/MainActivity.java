package com.kingeshop.heavendeer.databindingpractice;

import android.databinding.DataBindingUtil;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.kingeshop.heavendeer.databindingpractice.databinding.ActivityMainBinding;
import com.kingeshop.heavendeer.databindingpractice.userDetail.FirstFragment;


public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

       ActivityMainBinding activityMainBinding = DataBindingUtil.setContentView(this, R.layout.activity_main);
       setSupportActionBar(activityMainBinding.toolbar);
       getSupportActionBar().setDisplayHomeAsUpEnabled(true);


       getSupportFragmentManager().beginTransaction().add(R.id.frame_layout,new FirstFragment(),"firstLayout").commit();


    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }
}
