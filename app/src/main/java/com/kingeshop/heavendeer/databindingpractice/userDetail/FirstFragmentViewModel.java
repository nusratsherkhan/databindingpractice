package com.kingeshop.heavendeer.databindingpractice.userDetail;

import android.arch.lifecycle.MutableLiveData;
import android.arch.lifecycle.ViewModel;

import com.kingeshop.heavendeer.databindingpractice.networking.HeavenDeerClient;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by madan on 25/01/18.
 */

public class FirstFragmentViewModel  extends ViewModel{

    public MutableLiveData<List<UserObject>> data= new MutableLiveData<>();


    public void fetchData(){
        HeavenDeerClient.getRestClient().loadchat().enqueue(new Callback<List<UserObject>>() {
            @Override
            public void onResponse(Call<List<UserObject>> call, Response<List<UserObject>> response) {
                if (response.isSuccessful() && response.body()!=null){
                    data.setValue(response.body());

                }
            }

            @Override
            public void onFailure(Call<List<UserObject>> call, Throwable t) {

            }
        });

    }
}
